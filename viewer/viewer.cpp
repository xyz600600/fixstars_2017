#include <QApplication>
#include <QTextEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPainter>
#include <QColor>
#include <QKeyEvent>

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>
#include <cassert>
#include <set>
#include <queue>
#include <bitset>

using namespace std;

constexpr int BOARD_SIZE = 6;
constexpr int BOARD_SIZEx2 = BOARD_SIZE * BOARD_SIZE;

const bool DEBUG = false;

template<int N>
struct MyBitSet
{
  array<uint64_t, (N + 63) / 64> bitmap;
  size_t _size;
  
  array<uint64_t, (N + 63) / 64> bitmap_work;

  bool in_loop;
  int loop_index;

  constexpr int max_loop_index()
  {
	return (N + 63) / 64;
  }
  
  MyBitSet()
  {
	fill(bitmap.begin(), bitmap.end(), 0);
	in_loop = false;
	loop_index = 0;
	_size = 0;
  }

  void reset()
  {
	fill(bitmap.begin(), bitmap.end(), 0);
	_size = 0;
  }

  // for loop中にcopyが走らないと仮定
  void copy_from(const MyBitSet &src)
  {
	copy(src.bitmap.begin(), src.bitmap.end(), bitmap.begin());
	_size = src._size;
  }

  size_t size() const
  {
	return _size;
  }
  
  void set(const int g_pos, const bool val = true)
  {
	const int index = g_pos >> 6;
	const int l_pos = g_pos & 63;
	if (val)
	{
	  if ((bitmap[index] & (1ull << l_pos)) == 0)
	  {
		_size++;
	  }
	  bitmap[index] |= (1ull << l_pos);
	}
	else
	{
	  if ((bitmap[index] & (1ull << l_pos)) != 0)
	  {
		_size--;
	  }
	  bitmap[index] &= ~(1ull << l_pos);
	}
  }

  bool test(const int g_pos) const
  {
	const int index = g_pos >> 6;
	const uint64_t l_pos = (1ull << (g_pos & 63));
	return ((bitmap[index] & l_pos) == l_pos);
  }

  void init_loop()
  {
	copy(bitmap.begin(), bitmap.end(), bitmap_work.begin());
	in_loop = true;
	loop_index = 0;
  }

  int next_index()
  {
	if (!in_loop) init_loop();

	while (true)
	{
	  if (loop_index == max_loop_index())
	  {
		in_loop = false;
		return -1;
	  }
	  else if (bitmap_work[loop_index] != 0)
	  {
		const uint64_t pos = bitmap_work[loop_index] & (-bitmap_work[loop_index]);
		bitmap_work[loop_index] -= pos;
		return __builtin_ctzll(pos) + (loop_index << 6);
	  }
	  else
	  {
		loop_index++;
	  }
	}
  }
};

enum State
{
  BLACK_DISC,
  WHITE_DISC,
  QUANT_DISC,
  NONE,
};

State get_opposite_color(const State s)
{
  if (s == WHITE_DISC) return BLACK_DISC;
  else if (s == BLACK_DISC) return WHITE_DISC;
  else return s;
}

struct Board
{
  array<State, BOARD_SIZEx2> old_table;
  array<MyBitSet<BOARD_SIZEx2>, BOARD_SIZEx2> table;
  array<pair<int, int>, BOARD_SIZEx2> moves;
  int turn;

  Board();
  
  // 手を反映させる
  void set(const int pos1, const int pos2);

  // 手を削除する
  void undo();
  
  int to_index(const int y, const int x) const;
  
  char get_char(const State s) const;

  bool is_cyclic_entanglement(const int pos1, const int pos2);

  int count_old() const;
  
private:
  void set_old(const int t, const int pos);
  void flip(const int pos);
  
  void set_quant(const int pos1, const int pos2);
  
  bool is_cyclic_entanglement(const int pos1, const int pos2, bitset<BOARD_SIZEx2> &pos_set);
  void decide_quant(const int pos1, const int pos2, bitset<BOARD_SIZEx2> &pos_set);
  
  int get_opponent(const int t, const int pos) const;
  
  pair<int, int> to_yx(const int pos) const;
};

int Board::count_old() const
{
  int count = 0;
  for (int i = 0; i < BOARD_SIZEx2; i++)
  {
	if (old_table[i] == WHITE_DISC || old_table[i] == BLACK_DISC)
	{
	  count++;
	}
  }
  return count;
}

bool Board::is_cyclic_entanglement(const int pos1, const int pos2)
{
  bitset<BOARD_SIZEx2> pos_set;
  return is_cyclic_entanglement(pos1, pos2, pos_set);
}

char Board::get_char(const State s) const
{
  switch (s)
  {
  case BLACK_DISC: return 'x';
  case WHITE_DISC: return 'o';
  case QUANT_DISC: return '=';
  case NONE:       return '_';
  default: return 'e';
  }
}

ostream &operator<<(ostream &out, Board &b)
{
  out << "--------------------" << endl;
  out << "Step " << b.turn << endl;
  for (int y = 0; y < BOARD_SIZE; y++)
  {
	out << ";";
	for (int x = 0; x < BOARD_SIZE; x++)
	{
	  out << b.get_char(b.old_table[b.to_index(y, x)]);
	}
	out << endl;
  }
  out << "-----" << endl;
  for (int pos = 0; pos < BOARD_SIZEx2; pos++)
  {
	out << pos << ": ";
	int q_turn = b.table[pos].next_index();
	while (q_turn != -1)
	{
	  out << static_cast<int>(q_turn) << " ";
	  q_turn = b.table[pos].next_index();
	}
	out << endl;
  }
  out << "-----" << endl;
  out << "moves: [";
  for (int i = 0; i < b.turn; i++)
  {
	out << "(" << b.moves[i].first << ", " << b.moves[i].second << ")";
  }
  out << "]" << endl;
  return out;
}

pair<int, int> Board::to_yx(const int pos) const
{
  return make_pair(pos / BOARD_SIZE, pos % BOARD_SIZE);
}

int Board::to_index(const int y, const int x) const
{
  return y * BOARD_SIZE + x;
}

Board::Board()
{
  turn = 0;
  
  fill(old_table.begin(), old_table.end(), NONE);
  for (auto &v : table)
  {
	v.reset();
  }
  
  const int half = BOARD_SIZE / 2 - 1;
  set(to_index(half, half + 1), to_index(half, half + 1));
  set(to_index(half, half), to_index(half, half));
  set(to_index(half + 1, half), to_index(half + 1, half));
  set(to_index(half + 1, half + 1), to_index(half + 1, half + 1));
}

void Board::set(const int pos1, const int pos2)
{
  moves[turn] = make_pair(pos1, pos2);

  if (pos1 == pos2)
  {
	set_old(turn, pos1);
  }
  else
  {
	// 常に最新の手を打つ時にしか使われないので，turnは不要
	set_quant(pos1, pos2);
  }
  turn++;
}

array<int, 8> dy = {1, 1, 1, 0, -1, -1, -1, 0};
array<int, 8> dx = {1, 0, -1, -1, -1, 0, 1, 1};

void Board::flip(const int pos)
{
  int y, x;
  tie(y, x) = to_yx(pos);
  
  for (int i = 0; i < 8; i++)
  {
	int ey = y + dy[i];
	int ex = x + dx[i];
	while (0 <= ey && ey < BOARD_SIZE &&
		   0 <= ex && ex < BOARD_SIZE &&
		   get_opposite_color(old_table[pos]) == old_table[to_index(ey, ex)])
	{
	  ey += dy[i];
	  ex += dx[i];
	}
	if (0 <= ey && ey < BOARD_SIZE &&
		0 <= ex && ex < BOARD_SIZE &&
		old_table[pos] == old_table[to_index(ey, ex)])
	{
	  int ny = y + dy[i];
	  int nx = x + dx[i];
	  while (ny != ey || nx != ex)
	  {
		old_table[to_index(ny, nx)] = old_table[pos];
		ny += dy[i];
		nx += dx[i];
	  }
	}
  }
}

// 正式に手順を進める
void Board::set_old(const int t, const int pos)
{
  assert(table[pos].size() == 0);
  
  table[pos].set(t);
  old_table[pos] = (t % 2 == 0 ? WHITE_DISC : BLACK_DISC);

  flip(pos);
}

// 最初は pos2が選択されたと仮定
void Board::decide_quant(const int pos1, const int pos2, bitset<BOARD_SIZEx2> &pos_set)
{
  queue<pair<int, int>> q;
  q.push(make_pair(pos2, turn));

  vector<pair<int, int>> old_disk_list;
  
  while (!q.empty())
  {
	int pos, t;
	tie(pos, t) = q.front();
	pos_set.reset(pos);
	q.pop();

	int next_turn = table[pos].next_index();
	while (next_turn != -1)
	{
	  const int pos_another = get_opponent(next_turn, pos);
	  if (pos_set.test(pos_another) && next_turn != t)
	  {
		pos_set.reset(pos_another);
		q.push(make_pair(pos_another, next_turn));
	  }
	  next_turn = table[pos].next_index();
	}
	// pos を古典石にする
	table[pos].reset();
	old_disk_list.push_back(make_pair(t, pos));
	
	// pos の逆を消す
	const int pos_op = get_opponent(t, pos);
	table[pos_op].set(t, false);
  }
  sort(old_disk_list.begin(), old_disk_list.end(), greater<pair<int, int>>());
  
  for (auto turn_pos : old_disk_list)
  {
	int t, pos;
	tie(t, pos) = turn_pos;

	set_old(t, pos);
  }
}

int Board::get_opponent(const int t, const int pos) const
{
  return pos != moves[t].first ? moves[t].first : moves[t].second;
}

bool Board::is_cyclic_entanglement(const int pos1, const int pos2, bitset<BOARD_SIZEx2> &pos_set)
{
  queue<pair<int, int>> q;
  q.push(make_pair(pos2, turn));

  bool success = false;

  while (!q.empty())
  {
	int p, t;
	tie(p, t) = q.front();
	q.pop();
	pos_set.set(p);

	int next_turn = table[p].next_index();
	while (next_turn != -1)
	{
	  if (next_turn != t)
	  {
		const int p_op = get_opponent(next_turn, p);
		if (p_op == pos1)
		{
		  success = true;
		}
		if (!pos_set.test(p_op))
		{
		  q.push(make_pair(p_op, next_turn));
		}
	  }
	  next_turn = table[p].next_index();
	}
  }
  return success;
}

// 循環エンタングルメントが発生したら，pos2を選択
void Board::set_quant(const int pos1, const int pos2)
{
  assert(old_table[pos1] == QUANT_DISC || old_table[pos1] == NONE);
  assert(old_table[pos2] == QUANT_DISC || old_table[pos2] == NONE);
  
  table[pos1].set(turn);
  table[pos2].set(turn);

  old_table[pos1] = QUANT_DISC;
  old_table[pos2] = QUANT_DISC;
  
  bitset<BOARD_SIZEx2> pos_set;
  if (is_cyclic_entanglement(pos1, pos2, pos_set))
  {
	decide_quant(pos1, pos2, pos_set);
  }
}

// ============================================================
// Viewer

enum ViewerState
{
  FIRST_CHOICE,
  FIRST_QUANT_CHOICE,
  SECOND_CHOICE,
  SECOND_QUANT_CHOICE,
};

class QuantReversi : public QWidget
{
public:
  QuantReversi()
  {
	state = FIRST_CHOICE;
  }
  
protected:
  void paintEvent(QPaintEvent *);
  void mousePressEvent(QMouseEvent *event);
  void keyPressEvent(QKeyEvent *event);
  
private:
  Board board;

  ViewerState state;

  vector<int> selected;

  int move_pos1;
  int move_pos2;
  
  pair<int, int> get_ul_pos(int y_pos, int x_pos);
  pair<int, int> get_center_pos(int y_pos, int x_pos);

  void draw_x_pos(QPainter &painter, const int ul_x, const int ul_y,
				  const int width, const int height, const int turn,
				  const int div_size = 1);
  
  void draw_o_pos(QPainter &painter, const int ul_x, const int ul_y,
				  const int width, const int height, const int turn,
				  const int div_size = 1);
  
  void draw_q(QPainter &painter, int pos, int turn);
  void draw_o(QPainter &painter, int pos, int turn);
  void draw_x(QPainter &painter, int pos, int turn);
  void draw_outline(QPainter &painter);
  void draw_state(QPainter &painter);
  void draw_text(QPainter &painter);
  void draw_selected(QPainter &painter);
  
  int get_cell_size();
  pair<int, int> get_yx_pos(int y, int x);
  pair<int, int> get_text_pos();
  
  const int PADDING = 20;
  const int TEXT_PADDING = 100;
};

pair<int, int> QuantReversi::get_text_pos()
{
  return make_pair(height() - TEXT_PADDING / 2, PADDING);
}

int QuantReversi::get_cell_size()
{
  return (min(width(), height() - TEXT_PADDING) - PADDING * 2) / BOARD_SIZE;
}

pair<int, int> QuantReversi::get_ul_pos(int y_pos, int x_pos)
{
  const int y_coord = PADDING + (this->height() - TEXT_PADDING - 2 * PADDING) * y_pos / BOARD_SIZE;
  const int x_coord = PADDING + (this->width()  - 2 * PADDING) * x_pos / BOARD_SIZE;
  return make_pair(y_coord, x_coord);
}

pair<int, int> QuantReversi::get_center_pos(int y_pos, int x_pos)
{
  const int y_coord = PADDING + (this->height() - TEXT_PADDING - 2 * PADDING) * (y_pos + 0.5) / BOARD_SIZE;
  const int x_coord = PADDING + (this->width()  - 2 * PADDING) * (x_pos + 0.5) / BOARD_SIZE;
  return make_pair(y_coord, x_coord);
}

pair<int, int> QuantReversi::get_yx_pos(int y_pos, int x_pos)
{
  return make_pair((y_pos - PADDING) / get_cell_size(), (x_pos - PADDING) / get_cell_size());
}

void QuantReversi::keyPressEvent(QKeyEvent *event)
{
  if (event->key() == Qt::Key_Left)
  {
	update();
  }
  else if (event->key() == Qt::Key_Right)
  {
	update();
  }
}

void QuantReversi::mousePressEvent(QMouseEvent *event)
{
  if (PADDING <= event->x() && event->x() < this->width() - PADDING &&
	  PADDING <= event->y() && event->y() < this->height() - TEXT_PADDING - PADDING)
  {
	int y_pos, x_pos;
	tie(y_pos, x_pos) = get_yx_pos(event->y(), event->x());
	const int pos = y_pos * BOARD_SIZE + x_pos;

	if (state == FIRST_CHOICE || state == SECOND_CHOICE)
	{
	  auto it = std::find(selected.begin(), selected.end(), pos);
	  if (it == selected.end())
	  {
		if (board.old_table[pos] == QUANT_DISC || board.old_table[pos] == NONE)
		{
		  selected.push_back(pos);
		}
	  }
	  else if (board.count_old() < BOARD_SIZEx2 - 1)
	  {
		selected.erase(it);
	  }
	  
	  if (selected.size() == 1 && board.count_old() == BOARD_SIZEx2 - 1)
	  {
		move_pos1 = selected[0];
		move_pos2 = selected[0];
		board.set(move_pos1, move_pos2);
		cerr << "select: (" << move_pos1 << ", " << move_pos2 << ")" << endl;
	  }
	  else if (selected.size() == 2)
	  {
		move_pos1 = selected[0];
		move_pos2 = selected[1];
		cerr << "select: (" << move_pos1 << ", " << move_pos2 << ")" << endl;
		
		if (board.is_cyclic_entanglement(move_pos1, move_pos2))
		{
		  state = (state == FIRST_CHOICE ? SECOND_QUANT_CHOICE : FIRST_QUANT_CHOICE);
		}
		else
		{
		  selected.clear();
		  board.set(move_pos1, move_pos2);
		  state = (state == FIRST_CHOICE ? SECOND_CHOICE : FIRST_CHOICE);
		}
	  }
	}
	else 
	{
	  if (pos == move_pos1 || pos == move_pos2)
	  {
		if (move_pos1 == pos)
		{
		  swap(move_pos1, move_pos2);
		}
		cerr << "quant selected " << move_pos2 << endl;
		board.set(move_pos1, move_pos2);
		selected.clear();
		state = (state == SECOND_QUANT_CHOICE ? SECOND_CHOICE : FIRST_CHOICE);
	  }
	}
  }
  update();
}

void QuantReversi::draw_o_pos(QPainter &painter, const int ul_x, const int ul_y,
							  const int width, const int height, const int turn,
							  const int div_size)
{
  painter.setPen(QPen(QColor("red"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  
  painter.drawEllipse(ul_x, ul_y, width, height);
  
  painter.setPen(QPen(QColor("black"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  QFont font = painter.font();
  font.setPixelSize(44 / div_size);
  painter.setFont(font);
  const int cy = ul_y + height / 2;
  const int cx = ul_x + width / 2;
  
  painter.drawText(QPoint(cx, cy), to_string(turn).c_str());  
}

void QuantReversi::draw_o(QPainter &painter, int pos, int turn)
{
  const int y_pos = pos / BOARD_SIZE;
  const int x_pos = pos % BOARD_SIZE;
  int ul_y, ul_x;
  tie(ul_y, ul_x) = get_ul_pos(y_pos, x_pos);
  ul_y += PADDING;
  ul_x += PADDING;
  const int rad = get_cell_size() - PADDING * 2;
  
  draw_o_pos(painter, ul_x, ul_y, rad, rad, turn);
  painter.drawLine(ul_x, ul_y + rad + 5, ul_x + rad, ul_y + rad + 5);
}

void QuantReversi::draw_x_pos(QPainter &painter, const int ul_x, const int ul_y,
							  const int width, const int height, const int turn, 
							  const int div_size)
{
  painter.setPen(QPen(QColor("blue"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  
  const int r_x = ul_x + width;
  const int l_y = ul_y + height;
  
  painter.drawLine(ul_x, ul_y, r_x, l_y);
  painter.drawLine(ul_x, l_y, r_x, ul_y);

  painter.setPen(QPen(QColor("black"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  QFont font = painter.font();
  font.setPixelSize(44 / div_size);
  painter.setFont(font);
  const int cy = ul_y + height / 2;
  const int cx = ul_x + width / 2;
  painter.drawText(QPoint(cx, cy), to_string(turn).c_str());
}

void QuantReversi::draw_x(QPainter &painter, int pos, int turn)
{
  const int y_pos = pos / BOARD_SIZE;
  const int x_pos = pos % BOARD_SIZE;
  
  int sy, sx;
  tie(sy, sx) = get_ul_pos(y_pos, x_pos);
  sy += PADDING;
  sx += PADDING;

  const int rad = get_cell_size() - PADDING * 2;

  draw_x_pos(painter, sx, sy, rad, rad, turn);
  painter.drawLine(sx, sy + rad + 5, sx + rad, sy + rad + 5);
}

void QuantReversi::draw_q(QPainter &painter, int pos, int turn)
{
  const int div_size = ceil(sqrt(board.table[pos].size()));

  const int y_pos = pos / BOARD_SIZE;
  const int x_pos = pos % BOARD_SIZE;
  
  int sy, sx;
  tie(sy, sx) = get_ul_pos(y_pos, x_pos);
  sy += PADDING;
  sx += PADDING;

  const int rad = (get_cell_size() - PADDING * 2) / div_size;
  
  int next_index = board.table[pos].next_index();
  
  for (int r = 0; r < div_size; r++)
  {
	for (int c = 0; c < div_size; c++)
	{
	  if (next_index % 2 == 0)
	  {
		// draw o
		draw_o_pos(painter, sx + c * rad, sy + r * rad, rad, rad, next_index, div_size);
	  }
	  else
	  {
		// draw x
		draw_x_pos(painter, sx + c * rad, sy + r * rad, rad, rad, next_index, div_size);
	  }
	  
	  next_index = board.table[pos].next_index();
	  if (next_index == -1) return;
	}
  }
}

void QuantReversi::draw_outline(QPainter &painter)
{
  painter.setPen(QPen(QColor("black"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  
  // 外枠の表示
  for (int row = 0; row <= BOARD_SIZE; row++)
  {
	int ys, xs;
	tie(ys, xs) = get_ul_pos(row, 0);
	int ye, xe;
	tie(ye, xe) = get_ul_pos(row, 6);
	painter.drawLine(xs, ys, xe, ye);
  }
  
  for (int col = 0; col <= BOARD_SIZE; col++)
  {
	int ys, xs;
	tie(ys, xs) = get_ul_pos(0, col);
	int ye, xe;
	tie(ye, xe) = get_ul_pos(6, col);
	painter.drawLine(xs, ys, xe, ye);
  }
}

void QuantReversi::draw_state(QPainter &painter)
{
  // 中身の表示
  for (int row = 0; row < BOARD_SIZE; row++)
  {
	for (int col = 0; col < BOARD_SIZE; col++)
	{
	  const int pos = board.to_index(row, col);
	  int t = -1;
	  for (int i = 0; i < BOARD_SIZEx2; i++)
	  {
		if (board.table[pos].test(i))
		{
		  t = i;
		  break;
		}
	  }
	  switch(board.old_table[pos])
	  {
	  case BLACK_DISC:
		draw_x(painter, pos, t);
		break;
	  case WHITE_DISC:
		draw_o(painter, pos, t);
		break;
	  case QUANT_DISC:
		draw_q(painter, pos, t);
		break;
	  case NONE:
		break;
	  }
	}
  }
}

void QuantReversi::paintEvent(QPaintEvent *)
{
  QPainter painter(this);

  painter.setRenderHint(QPainter::Antialiasing, true); //アンチエイリアスセット
  draw_outline(painter);
  draw_state(painter);
  draw_text(painter);
  draw_selected(painter);
}

void QuantReversi::draw_selected(QPainter &painter)
{
  painter.setPen(QPen(QColor("yellow"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  
  for (int pos : selected)
  {
	const int y_pos = pos / BOARD_SIZE;
	const int x_pos = pos % BOARD_SIZE;
	
	int ys, xs;
	tie(ys, xs) = get_ul_pos(y_pos, x_pos);
	int ye, xe;
	tie(ye, xe) = get_ul_pos(y_pos + 1, x_pos);
	painter.drawLine(xs, ys, xe, ye);	

	tie(ys, xs) = get_ul_pos(y_pos + 1, x_pos);
	tie(ye, xe) = get_ul_pos(y_pos + 1, x_pos + 1);
	painter.drawLine(xs, ys, xe, ye);

	tie(ys, xs) = get_ul_pos(y_pos + 1, x_pos + 1);
	tie(ye, xe) = get_ul_pos(y_pos, x_pos + 1);
	painter.drawLine(xs, ys, xe, ye);
	
	tie(ys, xs) = get_ul_pos(y_pos, x_pos + 1);
	tie(ye, xe) = get_ul_pos(y_pos, x_pos);
	painter.drawLine(xs, ys, xe, ye);
  }
}

void QuantReversi::draw_text(QPainter &painter)
{
  painter.setPen(QPen(QColor("black"), 5, Qt::SolidLine, Qt::RoundCap, Qt::MiterJoin)); /*ペンセット*/
  QFont font = painter.font();
  font.setPixelSize(44);
  painter.setFont(font);

  int y, x;
  tie(y, x) = get_text_pos();
  
  switch(state)
  {
  case FIRST_CHOICE:
	painter.drawText(QPoint(x, y), "White(o) turn.");
	break;
  case FIRST_QUANT_CHOICE:
	painter.drawText(QPoint(x, y), "White(o) Cycle entanglement");
	break;
  case SECOND_CHOICE:
	painter.drawText(QPoint(x, y), "Black(x) turn.");
	break;
  case SECOND_QUANT_CHOICE:
	painter.drawText(QPoint(x, y), "Black(x) Cycle entanglement");
	break;
  }
}

int main(int argv, char *args[])
{
  QApplication app(argv, args);
  
  QWidget *window = new QuantReversi();
  window->resize(900, 1000);
  window->show();
  
  return app.exec();
}
