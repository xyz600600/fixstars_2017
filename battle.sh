#!/bin/sh

if [ -e accum.txt ]; then
	rm accum.txt
fi

NUM=20

for i in `seq 1 $NUM`
do
  echo $i
  python3 quantum_reversi.py sayama binary/sayama_v1 sayama2 binary/sayama_v4 > result.txt
  # python3 quantum_reversi.py tadashi "python tadashi.py" sayama2 ./a.out > result.txt
  # python3 quantum_reversi.py akemi ./akemi sayama ./a.out > result.txt
  tail -n 2 result.txt | head -n 1 >> accum.txt
done

for i in `seq 1 $NUM`
do
  echo $i
  python3 quantum_reversi.py sayama2 binary/sayama_v4 sayama binary/sayama_v1 > result.txt
  # python3 quantum_reversi.py sayama2 ./a.out tadashi "python tadashi.py" > result.txt
  # python3 quantum_reversi.py sayama ./a.out akemi ./akemi > result.txt
  tail -n 2 result.txt | head -n 1 >> accum.txt
done

# rm result.txt
python parse_result.py sayama2 sayama
