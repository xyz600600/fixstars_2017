#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <utility>
#include <cstdint>
#include <cassert>
#include <unordered_map>
#include <queue>
#include <iterator>
#include <chrono>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace std;

const int BOARD_SIZE = 6;
const int BOARD_SIZEx2 = BOARD_SIZE * BOARD_SIZE;

const int BOARD_WIDTH = BOARD_SIZE + 2;
const int BOARD_WIDTHx2 = BOARD_WIDTH * BOARD_WIDTH;

const bool DEBUG = false;

typedef long long ll;

int rest_time = 10000;
chrono::system_clock::time_point start_time;
chrono::system_clock::time_point end_time;

template<typename T, size_t MAX_SIZE>
class MyVector
{
private:
  array<T, MAX_SIZE> table;
  size_t _size;

public:

  // begin, end
  decltype(table.begin()) begin(){ return table.begin(); }
  const decltype(table.begin()) begin() const { return table.begin(); }
  decltype(table.end()) end(){ return table.begin() + _size; }
  const decltype(table.end()) end() const { return table.begin() + _size; }

  size_t size() const
  {
	return _size;
  }

  T &operator[](const int index)
  {
	return table[index];
  }

  T &back()
  {
	return table[_size - 1];
  }
  
  bool empty() const
  {
	return size() == 0;
  }

  void clear()
  {
	_size = 0;
  }
  
  void push_back(const T &val)
  {
	table[_size++] = val;
  }
  
  MyVector()
  {
	_size = 0;
  }
};

template<typename T>
class MyQueue
{
public:
  MyQueue()
  {
	front_index = 0;
	end_index = 0;
  }

  size_t size() const
  {
	return end_index - front_index;
  }

  bool empty() const
  {
	return end_index == front_index;
  }
  
  T front() const {
	return que[front_index];
  }

  void push(const T &val)
  {
	que[end_index] = val;
	end_index++;
  }

  void pop()
  {
	assert(front_index < end_index);
	front_index++;
  }

  void clear()
  {
	front_index = 0;
	end_index = 0;
  }
  
private:
  array<T, BOARD_SIZEx2> que;
  int front_index;
  int end_index;
};

struct MyUnionFind
{
  array<uint8_t, BOARD_WIDTHx2> root_array;
  
  MyUnionFind()
  {
	init();
  }

  void init()
  {
	for (int i = 0; i < root_array.size(); i++)
	{
	  root_array[i] = i;
	}
  }

  bool is_same(const int pos1, const int pos2)
  {
	return root(pos1) == root(pos2);
  }
  
  void uni(const int pos1, const int pos2)
  {
	int r1 = root(pos1);
	int r2 = root(pos2);
	root_array[r2] = r1;
  }
  
  uint8_t root(int pos)
  {
	if (root_array[pos] != pos)
	{
	  root_array[pos] = root(root_array[pos]);
	}
	return root_array[pos];
  }

  void copy_from(const MyUnionFind &src)
  {
	copy(src.root_array.begin(), src.root_array.end(), root_array.begin());
  }
};

typedef uint64_t BitBoard;

bool test_bm(const BitBoard board, const BitBoard pos)
{
  return (board & pos) == pos;
}

bool test(const BitBoard board, const int pos)
{
  return (board & (1ull << pos)) == (1ull << pos);
}

void set_bm(BitBoard &board, const BitBoard pos)
{
  board |= pos;
}

void set(BitBoard &board, const int pos)
{
  board |= (1ull << pos);
}

void reset(BitBoard &board)
{
  board = 0;
}

void reset_bm(BitBoard &board, const BitBoard pos)
{
  board &= ~pos;
}

void reset(BitBoard &board, const int pos)
{
  board &= ~(1ull << pos);
}

// 00000000
// 01000010
// 00000000
// 00000000
// 00000000
// 00000000
// 01000010
// 00000000
const BitBoard CORNER = 0x0042000000004200ull;

// 00000000
// 00111100
// 01000010
// 01000010
// 01000010
// 01000010
// 00111100
// 00000000
const BitBoard BORDER = 0x003c424242423c00ull;

const array<BitBoard, 4> BORDER_ARRAY = {
  0x003c000000000000,
  0x0000404040400000,
  0x0000020202020000,
  0x0000000000003c00,
};

void print_bitboard(const BitBoard &board)
{
  for (int y = 0; y < 8; y++)
  {
	for (int x = 0; x < 8; x++)
	{
	  cerr << ((board & (1ull << (y * 8 + x))) ? 'o' : '.');
	}
	cerr << endl;
  }
}

struct MyBitSet
{
  uint64_t bitmap;
  size_t _size;

  uint64_t bitmap_work;

  bool in_loop;
  
  MyBitSet()
  {
	bitmap = 0;
	bitmap_work = 0;
	in_loop = false;
	_size = 0;
  }

  // for loop中にcopyが走らないと仮定
  void copy_from(const MyBitSet &src)
  {
	bitmap = src.bitmap;
	_size = src._size;
  }

  size_t size() const
  {
	return _size;
  }
  
  void set(const int g_pos)
  {
	if ((bitmap & (1ull << g_pos)) == 0)
	{
	  _size++;
	  bitmap |= (1ull << g_pos);
	}
  }

  void reset(const int g_pos)
  {
	if ((bitmap & (1ull << g_pos)) != 0)
	{
	  _size--;
	  bitmap &= ~(1ull << g_pos);
	}
  }

  void reset()
  {
	bitmap = 0;
	_size = 0;
  }

  bool test(const int g_pos) const
  {
	return (bitmap & (1ull << g_pos)) == (1ull << g_pos);
  }

  int next_index()
  {
	if (!in_loop)
	{
	  bitmap_work = bitmap;
	  in_loop = true;
	}

	if (bitmap_work != 0)
	{
	  const uint64_t pos = bitmap_work & (-bitmap_work);
	  bitmap_work -= pos;
	  return __builtin_ctzll(pos);
	}
	else
	{
	  in_loop = false;
	  return -1;
	}
  }
};

enum State
{
  BLACK_DISC = 1,
  WHITE_DISC = 2,
  QUANT_DISC = 4,
  NONE = 8,
  WALL = 16,
};

int to_index_orig(const int y, const int x)
{
  return y * BOARD_SIZE + x;
}

pair<int, int> to_yx_orig(const int pos)
{
  return make_pair(pos / BOARD_SIZE, pos % BOARD_SIZE);
}

int to_index_inner(const int y, const int x)
{
  return (y << 3) + x;
}

pair<int, int> to_yx_inner(const int pos)
{
  return make_pair(pos >> 3, pos & 7);
}

int inner_to_orig(const int pos_inner)
{
  int y_inner, x_inner;
  tie(y_inner, x_inner) = to_yx_inner(pos_inner);
  y_inner--;
  x_inner--;
  return to_index_orig(y_inner, x_inner);
}

int orig_to_inner(const int pos_orig)
{
  int y_orig, x_orig;
  tie(y_orig, x_orig) = to_yx_orig(pos_orig);
  y_orig++;
  x_orig++;
  return to_index_inner(y_orig, x_orig);
}

struct Board
{
  BitBoard self_bitboard;
  BitBoard opponent_bitboard;
  MyUnionFind uf_pos;
  
  array<BitBoard, BOARD_SIZEx2> self_bitboard_array;
  array<BitBoard, BOARD_SIZEx2> opponent_bitboard_array;
  array<MyUnionFind, BOARD_SIZEx2> uf_pos_array;
  array<MyBitSet, BOARD_SIZEx2> entanglement;
  
  array<pair<int, int>, BOARD_SIZEx2> moves;
  array<MyBitSet, BOARD_WIDTHx2> table;

  int turn;

  int index;
  
  void init(const int index);
  
  void move(const int pos1, const int pos2);

  void pass();

  void undo_pass();
  
  void undo();
  
  bool is_cyclic_entanglement(const int pos1, const int pos2);
  
  char get_char(const State s) const;

  State get_state(const int pos) const;

private:
  void set_old(const int t, const int pos);
  
  void flip(const int pos, BitBoard &self_board, BitBoard &opponent_board);
  
  void set_quant(const int pos1, const int pos2);

  void decide_quant(const int pos1, const int pos2);

  int get_opponent(const int t, const int pos) const;
};

State Board::get_state(const int pos) const
{
  if (test(self_bitboard, pos))
  {
	if (index == 1) return WHITE_DISC;
	else return BLACK_DISC;
  }
  else if(test(opponent_bitboard, pos))
  {
	if (index == 1) return BLACK_DISC;
	else return WHITE_DISC;
  }
  else if(table[pos].size() > 0)
  {
	return QUANT_DISC;
  }
  else
  {
	int y, x;
	tie(y, x) = to_yx_inner(pos);
	if ((y == 0) || (y == BOARD_WIDTH - 1) || (x == 0) || (x == BOARD_WIDTH - 1))
	{
	  return WALL;
	}
	else
	{
	  return NONE;
	}
  }
}

char Board::get_char(const State s) const
{
  switch (s)
  {
  case BLACK_DISC: return 'o';
  case WHITE_DISC: return 'x';
  case QUANT_DISC: return '=';
  case NONE:       return '_';
  case WALL:       return '#';
  default: return 'e';
  }
}

ostream &operator<<(ostream &out, Board &b)
{
  out << "--------------------" << endl;
  out << "Step " << b.turn << endl;
  cerr << "self: 0x" << hex << b.self_bitboard << dec << endl;
  cerr << "oppo: 0x" << hex << b.opponent_bitboard << dec << endl;
  cerr << b.moves[b.turn - 1].first << ", " << b.moves[b.turn - 1].second << endl;
  
  for (int pos = 0; pos < BOARD_WIDTHx2; pos++)
  {
	cerr << static_cast<int>(b.uf_pos.root(pos)) << " ";
  }
  cerr << endl;

  for (int y = 0; y < BOARD_WIDTH; y++)
  {
	out << ";";
	for (int x = 0; x < BOARD_WIDTH; x++)
	{
	  out << b.get_char(b.get_state(to_index_inner(y, x)));
	}
	out << endl;
  }
  out << "-----" << endl;
  for (int pos = 0; pos < BOARD_WIDTHx2; pos++)
  {
	out << pos << ": ";
	int q_turn = b.table[pos].next_index();
	while (q_turn != -1)
	{
	  out << static_cast<int>(q_turn) << " ";
	  q_turn = b.table[pos].next_index();
	}
	out << endl;
  }
  out << "-----" << endl;
  out << "moves: [";
  for (int i = 0; i < b.turn; i++)
  {
	out << "(" << b.moves[i].first << ", " << b.moves[i].second << ")";
  }
  out << "]" << endl;
  return out;
}

// index: 0なら先行(黒 o)，1なら後攻(白 x)
void Board::init(const int index)
{
  turn = 0;
  this->index = index;
  self_bitboard = 0;
  opponent_bitboard = 0;
  uf_pos.init();

  const int half = BOARD_SIZE / 2;
  move(to_index_inner(half, half + 1)    , to_index_inner(half, half + 1));
  move(to_index_inner(half, half)        , to_index_inner(half, half));
  move(to_index_inner(half + 1, half)    , to_index_inner(half + 1, half));
  move(to_index_inner(half + 1, half + 1), to_index_inner(half + 1, half + 1));
}

void Board::undo()
{
  turn--;

  int next_index = entanglement[turn].next_index();
  while (next_index != -1)
  {
	table[moves[next_index].first].set(next_index);
	table[moves[next_index].second].set(next_index);
	
	next_index = entanglement[turn].next_index();
  }
  entanglement[turn].reset();
  
  table[moves[turn].first].reset(turn);
  table[moves[turn].second].reset(turn);
  
  self_bitboard = self_bitboard_array[turn];
  opponent_bitboard = opponent_bitboard_array[turn];

  uf_pos.copy_from(uf_pos_array[turn]);
}

void Board::undo_pass()
{
  turn--;
}

void Board::pass()
{
  turn++;
}

void Board::move(const int pos1, const int pos2)
{
  moves[turn] = make_pair(pos1, pos2);
  self_bitboard_array[turn] = self_bitboard;
  opponent_bitboard_array[turn] = opponent_bitboard;
  uf_pos_array[turn].copy_from(uf_pos);
  
  if (pos1 == pos2)
  {
	set_old(turn, pos1);
  }
  else
  {
	set_quant(pos1, pos2);
  }
  turn++;
}

// 周囲にpaddingをつけて，bit演算するだけで速くなるはず
array<int, 4> dp = {1, 9, 8, 7};

void Board::flip(const int pos, BitBoard &self_board, BitBoard &opponent_board)
{
  for (int i = 0; i < 4; i++)
  {
	// 正方向
	BitBoard ep_bm = (1ull << (pos + dp[i]));
	BitBoard rev_pos = 0;
	while (test_bm(opponent_board, ep_bm))
	{
	  rev_pos |= ep_bm;
	  ep_bm <<= dp[i];
	}
	if (rev_pos != 0 && test_bm(self_board, ep_bm))
	{
	  opponent_board ^= rev_pos;
	  self_board ^= rev_pos;
	}

	// 負方向
	ep_bm = (1ull << (pos - dp[i]));
	rev_pos = 0;
	while (test_bm(opponent_board, ep_bm))
	{
	  rev_pos |= ep_bm;
	  ep_bm >>= dp[i];
	}
	if (rev_pos != 0 && test_bm(self_board, ep_bm))
	{
	  opponent_board ^= rev_pos;
	  self_board ^= rev_pos;
	}
  }  
}

// 正式に手順を進める
void Board::set_old(const int t, const int pos)
{
  // assert(table[pos].size() == 0);

  table[pos].set(t);
  
  if ((t & 1) == index)
  {
	set(self_bitboard, pos);
	flip(pos, self_bitboard, opponent_bitboard);
  }
  else
  {
	set(opponent_bitboard, pos);
	flip(pos, opponent_bitboard, self_bitboard);
  }
}

void Board::decide_quant(const int pos1, const int pos2)
{
  // 最初は pos2が選択されたと仮定
  static MyVector<pair<int, int>, BOARD_WIDTHx2> old_disk_list;
  static MyQueue<pair<int, int>> que;
  
  // entanglementの設定
  MyBitSet pos_set;
  for (int pos = 0; pos < BOARD_WIDTHx2; pos++)
  {
	if (uf_pos.is_same(pos, pos1))
	{
	  pos_set.set(pos);
	  
	  int t = table[pos].next_index();
	  while (t != -1)
	  {
		entanglement[turn].set(t);
		t = table[pos].next_index();
	  }
	}
  }
  
  que.clear();
  que.push(make_pair(pos2, turn));

  old_disk_list.clear();
  
  while (!que.empty())
  {
	int pos, t;
	tie(pos, t) = que.front();
	pos_set.reset(pos);
	que.pop();

	int next_turn = table[pos].next_index();
	while (next_turn != -1)
	{
	  const int pos_another = get_opponent(next_turn, pos);
	  if (pos_set.test(pos_another) && next_turn != t)
	  {
		pos_set.reset(pos_another);
		que.push(make_pair(pos_another, next_turn));
	  }
	  next_turn = table[pos].next_index();
	}
	
	// pos を古典石にする
	table[pos].reset();
	old_disk_list.push_back(make_pair(t, pos));

	// pos の逆を消す
	const int pos_op = get_opponent(t, pos);
	table[pos_op].reset(t);
  }
  sort(old_disk_list.begin(), old_disk_list.end(), greater<pair<int, int>>());
  
  for (auto turn_pos : old_disk_list)
  {
	int t, pos;
	tie(t, pos) = turn_pos;

	set_old(t, pos);
  }
}

int Board::get_opponent(const int t, const int pos) const
{
  return pos != moves[t].first ? moves[t].first : moves[t].second;
}

bool Board::is_cyclic_entanglement(const int pos1, const int pos2)
{
  return uf_pos.is_same(pos1, pos2);
}

// 循環エンタングルメントが発生したら，pos2を選択
void Board::set_quant(const int pos1, const int pos2)
{
  assert(!test(self_bitboard, pos1) && !test(self_bitboard, pos2));
  assert(!test(opponent_bitboard, pos1) && !test(opponent_bitboard, pos2));

  table[pos1].set(turn);
  table[pos2].set(turn);

  if (is_cyclic_entanglement(pos1, pos2))
  {
	decide_quant(pos1, pos2);
  }
  else
  {
	entanglement[turn].reset();
	uf_pos.uni(pos1, pos2);
  }
}

// ========================================
// solver

pair<BitBoard, BitBoard> get_confirmed_border_stone(const BitBoard self_board, const BitBoard opponent_board)
{
  static const int SELF_INDEX = 0;
  static const int OPPO_INDEX = 1;

  static const int OLD_INDEX = 0;
  static const int CONFIRM_INDEX = 1;
  
  static array<array<BitBoard, 2>, 2> board;

  // 隅
  board[SELF_INDEX][OLD_INDEX] = self_board;
  board[SELF_INDEX][CONFIRM_INDEX] = self_board & CORNER;
  board[OPPO_INDEX][OLD_INDEX] = opponent_board;
  board[OPPO_INDEX][CONFIRM_INDEX] = opponent_board & CORNER;
  
  // 端
  for (int k = 0; k < 2; k++)
  {
	static const array<int, 4> corner_pos = {
	  to_index_inner(1, 1), to_index_inner(1, 6), to_index_inner(6, 1), to_index_inner(6, 6)
	};
	static const array<int, 4> corner_dy = {8, 8, -8, -8};
	static const array<int, 4> corner_dx = {1, -1, 1, -1};
	
	for (int i = 0; i < 4; i++)
	{
	  if (board[k][OLD_INDEX] & (1ull << corner_pos[i]))
	  {
		int pos = corner_pos[i] + corner_dy[i];
		while (board[k][OLD_INDEX] & (1ull << pos))
		{
		  board[k][CONFIRM_INDEX] |= (1ull << pos);
		  pos += corner_dy[i];
		}
		
		pos = corner_pos[i] + corner_dx[i];
		while (board[k][OLD_INDEX] & (1ull << pos))
		{
		  board[k][CONFIRM_INDEX] |= (1ull << pos);
		  pos += corner_dx[i];
		}
	  }
	}
  }
}

pair<BitBoard, BitBoard> get_confirmed_stone(const BitBoard self_board, const BitBoard opponent_board)
{
  static const int SELF_INDEX = 0;
  static const int OPPO_INDEX = 1;

  static const int OLD_INDEX = 0;
  static const int CONFIRM_INDEX = 1;
  
  static array<array<BitBoard, 2>, 2> board;

  // 隅
  board[SELF_INDEX][OLD_INDEX] = self_board;
  board[SELF_INDEX][CONFIRM_INDEX] = self_board & CORNER;
  board[OPPO_INDEX][OLD_INDEX] = opponent_board;
  board[OPPO_INDEX][CONFIRM_INDEX] = opponent_board & CORNER;
  
  // 端
  for (int k = 0; k < 2; k++)
  {
	static const array<int, 4> corner_pos = {
	  to_index_inner(1, 1), to_index_inner(1, 6), to_index_inner(6, 1), to_index_inner(6, 6)
	};
	static const array<int, 4> corner_dy = {8, 8, -8, -8};
	static const array<int, 4> corner_dx = {1, -1, 1, -1};
	
	for (int i = 0; i < 4; i++)
	{
	  if (board[k][OLD_INDEX] & (1ull << corner_pos[i]))
	  {
		int pos = corner_pos[i] + corner_dy[i];
		while (board[k][OLD_INDEX] & (1ull << pos))
		{
		  board[k][CONFIRM_INDEX] |= (1ull << pos);
		  pos += corner_dy[i];
		}
		
		pos = corner_pos[i] + corner_dx[i];
		while (board[k][OLD_INDEX] & (1ull << pos))
		{
		  board[k][CONFIRM_INDEX] |= (1ull << pos);
		  pos += corner_dx[i];
		}
	  }
	}
  }

  const BitBoard confirmed = board[OPPO_INDEX][CONFIRM_INDEX] | board[SELF_INDEX][CONFIRM_INDEX];
  
  // 中央
  for (int k = 0; k < 2; k++)
  {
	static const array<int, 12> center_pos = {
	  to_index_inner(2, 2), to_index_inner(2, 5), to_index_inner(5, 2), to_index_inner(5, 5),
	  to_index_inner(2, 3), to_index_inner(3, 2), to_index_inner(5, 4), to_index_inner(4, 5),
	  to_index_inner(4, 2), to_index_inner(5, 3), to_index_inner(2, 4), to_index_inner(3, 5)
	};
	
	for (int pos : center_pos)
	{
	  if (board[k][OLD_INDEX] & (1ull << pos))
	  {
		static const array<int, 4> dps = {1, BOARD_WIDTH - 1, BOARD_WIDTH, BOARD_WIDTH + 1};
		
		bool is_confirmed = true;
		for (int dp : dps)
		{
		  const BitBoard mask = (1ull << (pos + dp)) | (1ull << (pos - dp));
		  if ((confirmed & mask) != mask &&
			  (board[k][CONFIRM_INDEX] & mask) == 0)
		  {
			is_confirmed = false;
			break;
		  }
		}
		
		if (is_confirmed)
		{
		  board[k][CONFIRM_INDEX] |= 1ull << pos;
		}
	  }
	}
  }
  return make_pair(board[SELF_INDEX][CONFIRM_INDEX], board[OPPO_INDEX][CONFIRM_INDEX]);
}

double evaluate_border_with_penalty(Board &board)
{
  double eval = __builtin_popcountll(board.self_bitboard & BORDER) - __builtin_popcountll(board.opponent_bitboard & BORDER);
  
  array<BitBoard, 2> confirmed;
  // 確定石 + 量子石の情報は，confirmed に適宜書いていく
  tie(confirmed[0], confirmed[1]) = get_confirmed_border_stone(board.self_bitboard, board.opponent_bitboard);

  const array<BitBoard, 2> confirmed_init = { confirmed[0], confirmed[1] };

  static const array<int, 16> border_indices = {
	to_index_inner(1, 2), to_index_inner(1, 3), to_index_inner(1, 4), to_index_inner(1, 5),
	to_index_inner(2, 6), to_index_inner(3, 6), to_index_inner(4, 6), to_index_inner(5, 6),
	to_index_inner(6, 5), to_index_inner(6, 4), to_index_inner(6, 3), to_index_inner(6, 2),
	to_index_inner(5, 1), to_index_inner(4, 1), to_index_inner(3, 1), to_index_inner(2, 1)
  };

  static const array<int, 16> dp = {
	1, 1, 1, 1,
	BOARD_WIDTH, BOARD_WIDTH, BOARD_WIDTH, BOARD_WIDTH,
	1, 1, 1, 1,
	BOARD_WIDTH, BOARD_WIDTH, BOARD_WIDTH, BOARD_WIDTH
  };

  /**/
  auto index_of = [&](int pos) {
	for (int i = 0; i < 16; i++)
	{
	  if (pos == border_indices[i])
	  {
		return i;
	  }
	}
	return -1;
  };

  for (int t = 4; t < board.turn; t++)
  {
	const array<int, 2> indices = { index_of(board.moves[t].first), index_of(board.moves[t].second) };
	for (int idx : indices)
	{
	  if (idx == -1) continue;
	  
	  const BitBoard self = (t & 1 == 0 ? board.self_bitboard : board.opponent_bitboard);
	  const BitBoard confirmed_self = confirmed[t & 1];

	  // 量子石 or 確定じゃない古典石が置いてるなら減点対象
	  if (((1ull << border_indices[idx]) & BORDER) &&
		  board.get_state(border_indices[idx]) == QUANT_DISC)
	  {
		BitBoard opponent = confirmed[1 - (t & 1)];
		
		// 敵の確定石 or 若い量子石に挟まれていたら原点
		if (confirmed_init[1 - (t & 1)] & (1ull << (border_indices[idx] + dp[idx])) &&
			confirmed_init[1 - (t & 1)] & (1ull << (border_indices[idx] - dp[idx])))
		{
		  eval += (t & 1) ? 0.7 : -0.7;
		}
		else if (opponent & (1ull << (border_indices[idx] + dp[idx])) &&
				 opponent & (1ull << (border_indices[idx] - dp[idx])))
		{
		  eval += (t & 1) ? -0.7 : 0.7;
		}
	  }
	  if (board.get_state(border_indices[idx]) == QUANT_DISC)
	  {
		confirmed[t & 1] |= (1ull << border_indices[idx]);
	  }
	}
  }
  /**/
  
  return eval;  
}

double evaluate_confirmed_border_stone(Board &board)
{
  BitBoard self_confirmed, opponent_confirmed;
  tie(self_confirmed, opponent_confirmed) = get_confirmed_border_stone(board.self_bitboard, board.opponent_bitboard);

  return __builtin_popcountll(self_confirmed) - __builtin_popcountll(opponent_confirmed);
}	

double evaluate_confirmed_stone(Board &board)
{
  BitBoard self_confirmed, opponent_confirmed;
  tie(self_confirmed, opponent_confirmed) = get_confirmed_stone(board.self_bitboard, board.opponent_bitboard);

  return __builtin_popcountll(self_confirmed) - __builtin_popcountll(opponent_confirmed);
}	

double evaluate_only_stone(Board &board)
{
  return __builtin_popcountll(board.self_bitboard) - __builtin_popcountll(board.opponent_bitboard);
}

double evaluate_only_border(Board &board)
{
  return __builtin_popcountll(board.self_bitboard & BORDER) - __builtin_popcountll(board.opponent_bitboard & BORDER);
}

void expand_all(Board &board, MyVector<pair<int, int>, BOARD_WIDTHx2*BOARD_WIDTHx2> &children, mt19937 &mt)
{
  static MyVector<int, BOARD_SIZEx2> cand_single;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_quant;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_non_quant;
    
  cand_single.clear();
  cand_quant.clear();
  cand_non_quant.clear();
  
  for (int y = 1; y <= 6; y++)
  {
	for (int x = 1; x <= 6; x++)
	{
	  const int p_inner = to_index_inner(y, x);
	  if (!test(board.opponent_bitboard | board.self_bitboard, to_index_inner(y, x)))
	  {
		cand_single.push_back(to_index_inner(y, x));
	  }
	}
  }

  if (cand_single.size() == 1)
  {
	children.push_back(make_pair(cand_single[0], cand_single[0]));
  }
  else
  {
	for (int i = 0; i < cand_single.size(); i++)
	{
	  const int pos1 = cand_single[i];
	  for (int j = i + 1; j < cand_single.size(); j++)
	  {
		const int pos2 = cand_single[j];
  		if (board.is_cyclic_entanglement(pos1, pos2))
  		{
  		  cand_quant.push_back(make_pair(pos1, pos2));
  		}
  		else
  		{
  		  cand_non_quant.push_back(make_pair(pos1, pos2));
  		}
  	  }
  	}
  	shuffle(cand_quant.begin(), cand_quant.end(), mt);
  	shuffle(cand_non_quant.begin(), cand_non_quant.end(), mt);
  	for (int i = 0; i < cand_quant.size(); i++)
  	{
  	  children.push_back(cand_quant[i]);
  	}
  	for (int i = 0; i < cand_non_quant.size(); i++)
  	{
  	  children.push_back(cand_non_quant[i]);
  	}
  }
}

void expand_continuous_border(Board &board, MyVector<pair<int, int>, BOARD_WIDTHx2*BOARD_WIDTHx2> &children, mt19937 &mt)
{
  static const array<int, 20> border_indices = {
	to_index_inner(1, 1), to_index_inner(1, 2), to_index_inner(1, 3), to_index_inner(1, 4),
	to_index_inner(1, 5), to_index_inner(1, 6), to_index_inner(2, 6), to_index_inner(3, 6),
	to_index_inner(4, 6), to_index_inner(5, 6), to_index_inner(6, 6), to_index_inner(6, 5),
	to_index_inner(6, 4), to_index_inner(6, 3), to_index_inner(6, 2), to_index_inner(6, 1),
	to_index_inner(5, 1), to_index_inner(4, 1), to_index_inner(3, 1), to_index_inner(2, 1)
  };

  static MyVector<int, BOARD_SIZEx2> cand_single;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_quant;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_non_quant;
    
  cand_single.clear();
  cand_quant.clear();
  cand_non_quant.clear();
  
  for (int pos : border_indices)
  {
	if (!test((board.opponent_bitboard | board.self_bitboard), pos))
	{
	  cand_single.push_back(pos);
	}
  }
  if (cand_single.size() == 1)
  {
	children.push_back(make_pair(cand_single[0], cand_single[0]));
  }
  else if(cand_single.size() >= 2)
  {
	for (int i = 0; i < cand_single.size() - 1; i++)
	{
	  const int pos1 = cand_single[i];
	  const int pos2 = cand_single[i + 1];
	  if (board.is_cyclic_entanglement(pos1, pos2))
	  {
		cand_quant.push_back(make_pair(pos1, pos2));
	  }
	  else
	  {
		cand_non_quant.push_back(make_pair(pos1, pos2));
	  }
	}
	const int pos1 = cand_single.back();
	const int pos2 = cand_single[0];
	if (board.is_cyclic_entanglement(pos1, pos2))
	{
	  cand_quant.push_back(make_pair(pos1, pos2));
	}
	else
	{
	  cand_non_quant.push_back(make_pair(pos1, pos2));
	}
	shuffle(cand_quant.begin(), cand_quant.end(), mt);
	shuffle(cand_non_quant.begin(), cand_non_quant.end(), mt);
	for (int i = 0; i < cand_quant.size(); i++)
	{
	  children.push_back(cand_quant[i]);
	}
	for (int i = 0; i < cand_non_quant.size(); i++)
	{
	  children.push_back(cand_non_quant[i]);
	}
  }
}

void expand_border(Board &board, MyVector<pair<int, int>, BOARD_WIDTHx2*BOARD_WIDTHx2> &children, mt19937 &mt)
{
  static const array<array<int, 4>, 4> yss = {{
	  {1, 1, 1, 1},
	  {2, 3, 4, 5},
	  {6, 6, 6, 6},
	  {2, 3, 4, 5}
	}};
  
  static const array<array<int, 4>, 4> xss = {{
	  {2, 3, 4, 5},
	  {6, 6, 6, 6},
	  {2, 3, 4, 5},
	  {1, 1, 1, 1}
	}};

  static MyVector<int, BOARD_SIZEx2> cand_single;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_quant;
  static MyVector<pair<int, int>, BOARD_SIZEx2*BOARD_SIZEx2> cand_non_quant;
    
  cand_single.clear();
  cand_quant.clear();
  cand_non_quant.clear();
  
  for (int i = 0; i < 4; i++)
  {
	for (int j = 0; j < 4; j++)
	{
	  if (!test((board.opponent_bitboard | board.self_bitboard), to_index_inner(yss[i][j], xss[i][j])))
	  {
		cand_single.push_back(to_index_inner(yss[i][j], xss[i][j]));
	  }
	}
  }
  
  if (cand_single.size() == 1)
  {
	children.push_back(make_pair(cand_single[0], cand_single[0]));
  }
  else
  {
	for (int i = 0; i < cand_single.size(); i++)
	{
	  const int pos1 = cand_single[i];
	  for (int j = i + 1; j < cand_single.size(); j++)
	  {
		const int pos2 = cand_single[j];

		if (board.is_cyclic_entanglement(pos1, pos2))
		{
		  cand_quant.push_back(make_pair(pos1, pos2));
		}
		else
		{
		  cand_non_quant.push_back(make_pair(pos1, pos2));
		}
	  }
	}
	shuffle(cand_quant.begin(), cand_quant.end(), mt);
	shuffle(cand_non_quant.begin(), cand_non_quant.end(), mt);
	for (int i = 0; i < cand_quant.size(); i++)
	{
	  children.push_back(cand_quant[i]);
	}
	for (int i = 0; i < cand_non_quant.size(); i++)
	{
	  children.push_back(cand_non_quant[i]);
	}
  }
}

const int MAX_DEPTH = 6;

int counter = 0;

template<typename FUNC_EXPAND, typename FUNC_EVAL>
tuple<int, int, double> nega_alpha(Board &board, const int rest_depth, 
								const int alpha, const int beta, FUNC_EXPAND func_expand,
								FUNC_EVAL func_evaluate,
								uniform_real_distribution<double> &rand, mt19937 &mt)
{
  static array<MyVector<pair<int, int>, BOARD_WIDTHx2*BOARD_WIDTHx2>, BOARD_SIZEx2> children_array;
  
  if (rest_depth == 0)
  {
	counter++;
	double eval = func_evaluate(board);
	if ((board.turn % 2) != board.index) eval = -eval;

	return make_tuple(-1, -1, eval);
  }
  else
  {
	auto &children = children_array[rest_depth];
	children.clear();
	func_expand(board, children, mt);
	
	if (children.size() == 0)
	{
	  double eval = func_evaluate(board);
	  if ((board.turn % 2) != board.index) eval = -eval;
      
	  return make_tuple(-1, -1, eval);
	}
	
	int max_eval = alpha;
	int max_index = -1;
	int same_eval_count = 1;
	
	for (int i = 0; i < children.size(); i++)
	{
	  int pos1, pos2;
	  tie(pos1, pos2) = children[i];

	  board.move(pos1, pos2);
	  int eval = -get<2>(nega_alpha(board, rest_depth - 1, -beta, -max_eval,
									func_expand, func_evaluate, rand, mt));
	  board.undo();

	  if (board.is_cyclic_entanglement(pos1, pos2))
	  {
		board.move(pos2, pos1);
		int rev_eval = -get<2>(nega_alpha(board, rest_depth - 1, -beta, -max_eval,
										  func_expand, func_evaluate, rand, mt));
		if (eval > rev_eval ||
			(eval == rev_eval && rand(mt) > same_eval_count / (1.0 + same_eval_count)))
		{
		  swap(pos1, pos2);
		  eval = rev_eval;
		  if (eval == rev_eval)
		  {
			same_eval_count++;
		  }
		  else
		  {
			same_eval_count = 1;
		  }
		}
		board.undo();
	  }
	  
	  if (max_eval < eval ||
		  (eval == max_eval && rand(mt) > same_eval_count / (1.0 + same_eval_count)))
	  {
		max_eval = eval;
		max_index = i;
		if (eval == max_eval)
		{
		  same_eval_count++;
		}
		else
		{
		  same_eval_count = 1;
		}
	  }
	  if (max_eval >= beta)
	  {
		break;
	  }
	  if (rest_depth >= MAX_DEPTH - 1)
	  {
		end_time = chrono::system_clock::now();
		int diff = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
		if (rest_time - diff <= 1000)
		{
		  return make_tuple(children[max_index].first, children[max_index].second, max_eval);
		}
	  }
	}
	return make_tuple(children[max_index].first, children[max_index].second, max_eval);
  }
}

template<typename FUNC_EXPAND, typename FUNC_EVAL>
tuple<int, int, double> nega_alpha(Board &board, const int max_depth,
								FUNC_EXPAND func_expand,
								FUNC_EVAL func_evaluate,
								uniform_real_distribution<double> &rand,
								mt19937 &mt)
{
  return nega_alpha(board, min<int>(max_depth, BOARD_SIZEx2 - board.turn), -10000000, 10000000, func_expand, func_evaluate, rand, mt);
}

int get_corner_size(const Board &b)
{
  return 4 - __builtin_popcountll(CORNER & (b.self_bitboard | b.opponent_bitboard));
}

int get_border_size(const Board &b)
{
  return 16 - __builtin_popcountll(BORDER & (b.self_bitboard | b.opponent_bitboard));
}

const array<int, 4> ys = {1, 6, 6, 1};
const array<int, 4> xs = {1, 1, 6, 6};

void get_corner_move(const Board &b, const int num, vector<int> &vs)
{
  vs.clear();

  for (int i = 0; i < 4; i++)
  {
	if (b.get_state(to_index_inner(ys[i], xs[i])) & (QUANT_DISC | NONE))
	{
	  vs.push_back(to_index_inner(ys[i], xs[i]));
	  if (vs.size() == num) return;
	}
  }
}

tuple<int, int, double> get_corner_and_next(const Board &b)
{
  vector<int> vs;
  get_corner_move(b, 1, vs);

  for (int j = 1; j <= 2; j++)
  {
	for (int i = 0; i < 4; i++)
	{
	  if (test(b.self_bitboard, to_index_inner(ys[i], xs[i])))
	  {
		const int next_x = (xs[i] == 6 ? (6 - j) : (1 + j));
		if (b.get_state(to_index_inner(ys[i], next_x)) & (QUANT_DISC | NONE))
		{
		  return make_tuple(vs[0], to_index_inner(ys[i], next_x), 0.0);
		}
		const int next_y = (ys[i] == 6 ? (6 - j) : (1 + j));
		if (b.get_state(to_index_inner(next_y, xs[i])) & (QUANT_DISC | NONE))
		{
		  return make_tuple(vs[0], to_index_inner(next_y, xs[i]), 0.0);
		}
	  }
	}
  }
  assert(false);
}

// pos1, pos2, eval
tuple<int, int, double> solve(Board &b, mt19937 &mt)
{
  uniform_real_distribution<double> rand;
  
  if (get_corner_size(b) >= 2)
  {
  	// 4隅が空いていたら，4隅を取る
  	vector<int> vs;
  	get_corner_move(b, 2, vs);
  	return make_tuple(vs[0], vs[1], 0);
  }
  else if (get_corner_size(b) == 1)
  {
  	// 4隅が1個だけ空いていたら，4隅 + 自分の確定隅の隣りに置く
  	return get_corner_and_next(b);
  }
  else if (get_border_size(b) > MAX_DEPTH)
  {
	// 全探索して端の個数が一番多いものを選択
 	return nega_alpha(b, MAX_DEPTH, expand_continuous_border, evaluate_border_with_penalty, rand, mt);
	// return nega_alpha(b, MAX_DEPTH, expand_continuous_border, evaluate_only_border, rand, mt);
  }
  else if (2 <= get_border_size(b) && get_border_size(b) <= MAX_DEPTH)
  {
	// 全探索して端の個数が一番多いものを選択
	return nega_alpha(b, MAX_DEPTH, expand_border, evaluate_only_border, rand, mt);
  }
  else if (b.turn + MAX_DEPTH < BOARD_SIZEx2)
  {
	// 端を取り切ったら，4手読みして一番確定石が多いのを選択
	return nega_alpha(b, 4, expand_all, evaluate_confirmed_stone, rand, mt);
  }
  else
  {
	// 最終 MAX_DEPTH 手を切ったら，全探索して最良の手を選択
	return nega_alpha(b, MAX_DEPTH, expand_all, evaluate_only_stone, rand, mt);
  }
}

int select_cyclic_entanglement(Board &b, const int pos1, const int pos2, mt19937 &mt)
{
  b.move(pos2, pos1);
  int _, __, eval1;
  tie(_, __, eval1) = solve(b, mt);
  b.undo();
  
  b.move(pos1, pos2);
  int eval2;
  tie(_, __, eval2) = solve(b, mt);
  b.undo();
  
  // 相手のturnとみなせるので，通常の探索とは逆
  return (eval1 > eval2 ? pos1 : pos2);
}

// ====================
// for test

int c = 0;

void setup_test()
{
  uniform_real_distribution<double> rand;
  mt19937 mt;
  
  const int AB_TURN = MAX_DEPTH;

  for (int max_d = 1; max_d <= AB_TURN; max_d++)
  {
	Board b;
	b.init(0);

	int p = 0;
	
	while (b.turn < BOARD_SIZEx2 - max_d)
	{
	  array<int, 2> pos;
	  int count = 0;
	  while (true)
	  {
		const int p_inner = orig_to_inner(p);
		if (((b.self_bitboard | b.opponent_bitboard) & (1ull << p_inner)) != (1ull << p_inner))
		{
		  pos[count] = p_inner;
		  count++;
		}
		p++;
		if (p == BOARD_SIZEx2) p = 0;
		if (count == 2) break;
	  }
	  b.move(pos[0], pos[1]);
	}
	
	cerr << get<2>(nega_alpha(b, max_d, expand_all, evaluate_only_stone, rand, mt)) << endl;
	cerr << "count = " << counter << endl;
  }
}

// ========================================

void send_nop()
{
  cout << endl;
}

// 標準入力で来るjsonから，自分+相手の手を取る
// 自分の循環エンタングルメントの選択とかも記憶すると分岐が面倒なので，2手分を読む
void get_latest_move(const boost::property_tree::ptree &pt, vector<pair<uint64_t, uint64_t>> &move_list)
{
  vector<tuple<int, int, int>> pos_list;
  for (auto &vs : pt.get_child("moves"))
  {
	const int select = vs.second.back().second.get_value<int>();
	vector<int> vals;
	for (auto &cell : vs.second)
	{
	  for (auto &v : cell.second)
	  {
		vals.push_back(v.second.get_value<int>());
	  }
	  break;
	}
	pos_list.push_back(make_tuple(select, vals[0], vals[1]));
  }
  
  reverse(pos_list.begin(), pos_list.end());
  move_list.clear();
  for (int i = 0; i < 2; i++)
  {
	const uint64_t pos1 = orig_to_inner(get<1>(pos_list[i]));
	const uint64_t pos2 = orig_to_inner(get<2>(pos_list[i]));
	
	if (get<0>(pos_list[i]) == 0)
	{
	  move_list.push_back(make_pair(pos2, pos1));
	}
	else
	{
	  move_list.push_back(make_pair(pos1, pos2));
	}
  }
}

size_t get_turn(const boost::property_tree::ptree &pt)
{
  return pt.get_child("moves").size();
}

size_t get_index(const boost::property_tree::ptree &pt)
{
  return pt.get<int>("index");
}

pair<int, int> get_entanglement(const boost::property_tree::ptree &pt)
{
  std::vector<int> entanglement;
  for (auto& x : pt.get_child("entanglement")) {
	entanglement.push_back(std::stol(x.second.data()));
  }
  return make_pair(entanglement[0], entanglement[1]);
}

// 送信用データを1行にまとめる
void remove_newline(std::string& s)
{
  std::string target("\n");
  std::string::size_type pos = s.find(target);
  while (pos != std::string::npos) {
	s.replace(pos, target.size(), "");
	pos = s.find(target, pos);
  }
}

void send_answer(const pair<int, int> &pos1_pos2)
{
  boost::property_tree::ptree pto;
  boost::property_tree::ptree positions_node;
  
  boost::property_tree::ptree position_node1;
  position_node1.put("", inner_to_orig(pos1_pos2.first));
  positions_node.push_back(std::make_pair("", position_node1));

  boost::property_tree::ptree position_node2;
  position_node2.put("", inner_to_orig(pos1_pos2.second));
  positions_node.push_back(std::make_pair("", position_node2));

  pto.add_child("positions", positions_node);

  stringstream ss;
  boost::property_tree::write_json(ss, pto);
  
  std::string json(ss.str());
  remove_newline(json);
  std::cout << json << std::endl;
}

void send_select(const int pos_inner)
{
  boost::property_tree::ptree pto;
  pto.put("select", inner_to_orig(pos_inner));
  
  stringstream ss;
  boost::property_tree::write_json(ss, pto);
  std::string json(ss.str());
  remove_newline(json);
  std::cout << json << std::endl;
}

// ====================

int main()
{
  Board board;
  
  random_device seed_gen;
  auto seed = seed_gen();
  cerr << "seed = " << seed << endl;
  mt19937 mt(seed);
   
  while (true)
  {
  	string line;
  	getline(cin, line);
  	stringstream ss(line);
  	boost::property_tree::ptree pt;
  	boost::property_tree::read_json(ss, pt);
  	auto action = pt.get<std::string>("action");

	start_time = chrono::system_clock::now();
	
  	if (action == "play")
  	{
	  const int turn = get_turn(pt);
	  if (turn >= 6)
	  {
		vector<pair<uint64_t, uint64_t>> move_list;
		get_latest_move(pt, move_list);
		board.move(move_list[1].first, move_list[1].second);
		board.move(move_list[0].first, move_list[0].second);
	  }
	  else if (turn == 5)
	  {
		vector<pair<uint64_t, uint64_t>> move_list;
		get_latest_move(pt, move_list);
		board.move(move_list[0].first, move_list[0].second);
	  }
	  int pos1, pos2, _;
	  tie(pos1, pos2, _) = solve(board, mt);
	  send_answer(make_pair(pos1, pos2));
  	}
  	else if (action == "select")
  	{
	  vector<pair<uint64_t, uint64_t>> move_list;
	  get_latest_move(pt, move_list);
	  board.move(move_list[1].first, move_list[1].second);
	  
	  auto candidate = get_entanglement(pt);
	  const int select = select_cyclic_entanglement(board, orig_to_inner(candidate.first), orig_to_inner(candidate.second), mt);
	  send_select(select);
	  board.undo();
  	}
  	else if (action == "init")
  	{
	  // 必要なら，moveを最初ここから復元する
	  const int index = get_index(pt);
	  board.init(index);
  	  send_nop();
  	}
  	else if (action == "quit")
  	{
  	  send_nop();
	  break;
  	}
	end_time = chrono::system_clock::now();
	rest_time -= std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
  }
}
