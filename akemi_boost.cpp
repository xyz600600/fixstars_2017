// akemi's Quantum Reversi Solver (C) 2017 Fixstars Corp.
// g++ akemi_boost.cpp -std=c++14 -o akemi_boost -O3 -Wall -Wno-unused-but-set-variable

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <utility>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

constexpr const char* version  = "0.06";
constexpr const char* revision = "a";
constexpr const char* ver_date = "20171201";

std::pair<int, int> pos2xy(int idx, int w)
{
	return std::make_pair(idx % w, idx / w);
}

int xy2pos(int x, int y, int w)
{
	return x + y * w;
}

void remove_newline(std::string& s)
{
	std::string target("\n");
	std::string::size_type pos = s.find(target);
	while (pos != std::string::npos) {
		s.replace(pos, target.size(), "");
		pos = s.find(target, pos);
	}
}

void solver(std::vector<int>& cells, int w, int h)
{
	std::sort(cells.begin(), cells.end(), [w, h](int a, int b) {
			auto xy1 = pos2xy(a, w);
			auto xy2 = pos2xy(b, w);
			auto f = [](int a, int b){ int x = a - b; return x < 0 ? ~x : x; };
			int p1 = f(xy1.first, w / 2) + f(xy1.second, h / 2);
			int p2 = f(xy2.first, w / 2) + f(xy2.second, h / 2);
			return p1 > p2;
		});
}

int main(int argc, char* argv[])
{
	std::string name, opp_name;
	std::string s;
	std::stringstream ss;
	std::vector<std::string> data;
	std::vector<int> wh;

	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());
	std::uniform_int_distribution<> coin(0, 1);

	std::string white_disc = "o";
	std::string black_disc = "x";
	std::string quantum_disc = "=";
	std::string empty_disc = " ";

	for (;;) {
		getline(std::cin, s);
		ss.str(s);
		boost::property_tree::ptree pt;
		boost::property_tree::read_json(ss, pt);

		std::string action(pt.get<std::string>("action"));
			
		if (action == "play") {
			std::vector<std::string> board;
			for (auto& x : pt.get_child("board")) {
				//board.push_back(x.second.data().at(0));
				board.push_back(x.second.data());
			}
			std::vector<int> cells;
			for (int i = 0; i < static_cast<int>(board.size()); i++) {
				std::string c = board[i];
				if (c != white_disc && c != black_disc) cells.push_back(i);
			}
			
			//std::shuffle(cells.begin(), cells.end(), engine);
			solver(cells, wh[0], wh[1]);
			
			boost::property_tree::ptree pto;
			boost::property_tree::ptree positions_node;
			for (int i = 0; i < 2; i++) {
				boost::property_tree::ptree position_node;
				position_node.put("", cells[cells.size() > 1 ? i : 0]);
				positions_node.push_back(std::make_pair("", position_node));
			}
			pto.add_child("positions", positions_node);

			ss.str("");
			boost::property_tree::write_json(ss, pto);
			std::string json(ss.str());
			remove_newline(json);
			std::cout << json << std::endl << std::flush;

		} else if (action == "select") {
			std::vector<int> entanglement;
			for (auto& x : pt.get_child("entanglement")) {
				entanglement.push_back(std::stol(x.second.data()));
			}
			int select = entanglement[coin(engine)];
			boost::property_tree::ptree pto;
			pto.put("select", select);

			ss.str("");
			boost::property_tree::write_json(ss, pto);
			std::string json(ss.str());
			remove_newline(json);
			std::cout << json << std::endl << std::flush;

		} else if (action == "init") {
			int index = pt.get<int>("index");
			std::vector<std::string> names;
			for (auto& x : pt.get_child("names")) {
				names.push_back(x.second.data());
			}
			std::string name(names[index]);
			for (auto& x : pt.get_child("size")) {
				wh.push_back(std::stol(x.second.data()));
			}
			white_disc = pt.get<std::string>("white");
			black_disc = pt.get<std::string>("black");
			quantum_disc = pt.get<std::string>("quantum");
			empty_disc = pt.get<std::string>("empty");
			std::cout << std::endl << std::flush;

		} else if (action == "quit") {
			std::cout << std::endl << std::flush;
			break;
		}
	}
	
	return 0;
}
