# -*- coding: utf-8 -*-

import re
import sys

def get_score(line, name):
    pat = re.compile(name + ": ([0-9]+)")
    return int(pat.findall(line)[0])

name1 = sys.argv[1]
name2 = sys.argv[2]
    
name1_result = []
name2_result = []

def mean(lst):
    return sum(lst) * 1.0 / len(lst)

for i, line in enumerate(open('accum.txt').readlines()):
    name1_result.append(get_score(line, name1))
    name2_result.append(get_score(line, name2))

print name1, "_mean: ", mean(name1_result)
print name2, "_mean: ", mean(name2_result)
win_num =  len(filter(lambda x: x[0] > x[1], zip(name1_result, name2_result)))
lose_num = len(filter(lambda x: x[0] < x[1], zip(name1_result, name2_result)))
draw_num = len(filter(lambda x: x[0] == x[1], zip(name1_result, name2_result)))
print "result: %d - %d - %d" % (win_num, lose_num, draw_num)
